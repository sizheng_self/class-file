//
// Created by ouka on 2021/12/17.
//

#ifndef _STRINGUTIL_H
#define _STRINGUTIL_H

#define OUKA ouka
#define true  1
#define false 0
typedef char *String;
typedef char **Array_t;
typedef unsigned char Bool;

typedef struct {
    char *(*newString2)(int,...);
} OUKA;


extern OUKA oukalala;
extern OUKA ouka2;

typedef struct {
    char *(*addExtra)(char *, char *);

    char *(*add)(char *, char *);

    char *(*newString)(int, ...);

    void (*delString)(char *);

    int (*split)(char *, char *, Array_t *);

    int (*splitExtra)(char *, char *, Array_t *);

    void (*delArray)(Array_t, int);

    char *(*toUpper)(char *);

    char *(*toLower)(char *);

    Bool (*startWith)(char *, char *);

    Bool (*endWith)(char *, char *);

    char *(*join)(Array_t, int);

    char *(*strip)(char *, char *);


} STRINGUTIL;
extern STRINGUTIL StringUtil;


void stringUtilTest(void);


#endif
